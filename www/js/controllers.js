var pago_movil = angular.module('pago_movil.controllers', ['ionic', 'ngCordova']);

pago_movil.controller('InicioCtrl', function ($scope, $state) {
  $scope.title = 'Pago Nets'
  $scope.data = {
    items: [
      {'id': 1, 'titulo': 'Pagar', 'url':'#/app/pagar'},
      {'id': 2, 'titulo': 'Recibir Pago', 'url':'#/app/recibir'},
      {'id': 3, 'titulo': 'Transacciones', 'url':'#/app/transacciones'},
      {'id': 4, 'titulo': 'Cuentas', 'url':'#/app/cuentas'},
      {'id': 5, 'titulo': 'Asociar Cuenta', 'url':'#/app/asociar'},
      {'id': 6, 'titulo': 'Generar', 'url':'#/app/generar'}
    ]
  };
  
  $scope.crear = function () {
    $state.go('create');
  }
  
  $scope.inicar = function () {
    $state.go('sincronizar');
  }
})


pago_movil.controller('SincronizarCtrl', function ($scope, $state, $stateParams, $cordovaBarcodeScanner, $ionicPopup, usersFactory) {
  $scope.escanear = function () {
    
    var texto = 'qMj6X7iPhOgqy0dQ4CcAcvQZEC8';
    texto = {'hash': texto};
    usersFactory.loginHash(texto).success(function (response, status) {
      if (response.code == '200') {
        localStorage.setItem("user_id", response.id);
        localStorage.setItem("saldo", response.saldo);
        $state.go('app.cuentas');
      } else if (response.code == '400') {
        var alertPopup = $ionicPopup.alert({
          title: 'ERROR',
          template: response.message
        });
        alertPopup.then(function (res) {
          console.log('Thank you for not eating my delicious ice cream cone');
        });
        
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'ERROR',
          template: response.message
        });
        alertPopup.then(function (res) {
          console.log('Thank you for not eating my delicious ice cream cone');
        });
      }
    }, 'json');
    
    /*document.addEventListener("deviceready", function () {
      
      $cordovaBarcodeScanner
        .scan()
        .then(function (barcodeData) {
          var texto = barcodeData.text;
          texto = {'hash': texto};
          usersFactory.loginHash(texto).success(function (response, status) {
            if (response.code == '200') {

              $state.go('app.cuentas');
            }
          }, 'json');
        }, function (error) {
        });
    }, false);*/
  }
  
  $scope.login = function () {
    $state.go('login');
  }
})

pago_movil.controller('AppCtrl', function ($scope, $state, $ionicPopup, usersFactory, Data) {
  
  $scope.loginData = {};
  
  $scope.doLogin = function () {
    
    usersFactory.loginUser($scope.loginData).success(function (response, status) {
      if (response.code == '200') {
        localStorage.setItem("user_id", response.id);
        localStorage.setItem("saldo", response.saldo);
        $state.go('app.cuentas');
      } else if (response.code == '400') {
        var alertPopup = $ionicPopup.alert({
          title: 'ERROR',
          template: response.message
        });
        alertPopup.then(function (res) {
          console.log('Thank you for not eating my delicious ice cream cone');
        });
        
      } else {
        var alertPopup = $ionicPopup.alert({
          title: 'ERROR',
          template: response.message
        });
        alertPopup.then(function (res) {
          console.log('Thank you for not eating my delicious ice cream cone');
        });
      }
    }, 'json').error(function (data, status) {
      console.log('Error', status, data);
    });
  };
  
  
  $scope.createData = {};
  $scope.doCrear = function () {
    usersFactory.createUser($scope.createData).success(function (response, status) {
      var alertPopup = $ionicPopup.alert({
        title: 'MENSAJE',
        template: response.message
      })
      if (response.code == '200') {
        Data.setEmail($scope.createData.email);
        $state.go('confirmation');
      }
    }, 'json')
  };
})

pago_movil.controller('ConfirmarCtrl', function ($scope, $state, Data) {
  $scope.email_corr = Data.getEmail();
  $scope.next = function () {
    $state.go('app.ini');
  };
  
})

pago_movil.controller('IniCtrl', function ($scope, $http, usersFactory, Data) {

})

pago_movil.controller('CuentasCtrl', function ($scope, $http, usersFactory) {
  $scope.user_id = localStorage.getItem("user_id");
  $scope.saldo = localStorage.getItem("saldo");

  usersFactory.getCuentas($scope.user_id).success(function (response, status) {
    if (response.data != 0) {
      $scope.cuentas = response.data.cuentas;
    }
  }, 'json');
  
  $scope.sort = function (keyname) {
    $scope.sortKey = keyname;   //set the sortKey to the param passed
    $scope.reverse = !$scope.reverse; //if true make it false and vice versa
  }
})

pago_movil.controller('PagarCtrl', function ($scope, $state, $stateParams, $cordovaBarcodeScanner, $ionicPopup, usersFactory) {
  $scope.user_id = localStorage.getItem("user_id");
  $scope.saldo = localStorage.getItem("saldo");
  
  $scope.doPagar = function (datos) {
    texto = $scope.user_id + ';' + Object.keys(datos).map(function (k) {
      return datos[k]
    }).join(";");
    
    texto = {'datos': texto};
    usersFactory.transaccionPagar(texto).success(function (response, status) {
      
      var alertPopup = $ionicPopup.alert({
        title: 'MENSAJE',
        template: response.data.message
      });
      alertPopup.then(function (res) {
        
        localStorage.setItem("saldo", response.data.saldo);
        
        $state.go('app.cuentas', null, {reload: true});
        // $state.go('app.cuentas');
      });
    }, 'json');
  }
  $scope.lector = function (datos) {
    
    document.addEventListener("deviceready", function () {
      
      $cordovaBarcodeScanner
        .scan()
        .then(function (barcodeData) {
          $scope.user_id = localStorage.getItem("user_id");
          var texto = barcodeData.text;
          var datos = texto.split(';');
          texto = {'datos': $scope.user_id + ';' + texto};
          var confirmPopup = $ionicPopup.confirm({
            title: 'PAGAR',
            template: "Pagara a:" + datos[1] + " Un total de:" + datos[0]
          });
          confirmPopup.then(function (res) {
            if (res) {
              usersFactory.transaccionPagar(texto).success(function (response, status) {
                var alertPopup = $ionicPopup.alert({
                  title: 'MENSAJE',
                  template: response.data.message
                });
              }, 'json');
            }
          });
        }, function (error) {
        });
    }, false);
  }
});

pago_movil.controller('AsociarCtrl', function ($scope, $state, $stateParams, $cordovaBarcodeScanner, $ionicPopup, usersFactory) {
  
  $scope.lector = function (datos) {
    document.addEventListener("deviceready", function () {
      $cordovaBarcodeScanner
        .scan()
        .then(function (barcodeData) {
          $scope.user_id = localStorage.getItem("user_id");
          var texto = barcodeData.text;
          var datos = texto.split(';');
          var cuenta = {'cuenta': $scope.user_id + ';' + texto};
          var confirmPopup = $ionicPopup.confirm({
            title: 'PAGAR',
            template: "¿Desea asociar la cuenta a pago movil?"
          });
          confirmPopup.then(function (res) {
            if (res) {
              usersFactory.asociarCuenta(cuenta).success(function (response, status) {
                var alertPopup = $ionicPopup.alert({
                  title: 'MENSAJE',
                  template: response.data.message
                });
              }, 'json');
            }
          });
        }, function (error) {
        });
    }, false);
  }
});

pago_movil.controller('GenerarCtrl', function ($scope, $cordovaBarcodeScanner) {
  
  $scope.generar = function (datos) {
    document.addEventListener("deviceready", function () {
      
      
      // NOTE: encoding not functioning yet
      $cordovaBarcodeScanner
        .encode(BarcodeScanner.Encode.TEXT_TYPE, "http://www.nytimes.com")
        .then(function (success) {
          alert('fff')
        }, function (error) {
          // An error occurred
        });
      
    }, false);
  }
});

pago_movil.controller('TransaccionesCtrl', function ($scope, $http, usersFactory, $ionicActionSheet) {
  $scope.showActionsheet = function () {
    
    $ionicActionSheet.show({
      titleText: 'ActionSheet Example',
      cancelText: 'Cancelado',
      cancel: function () {
        console.log('CANCELLED');
      },
      destructiveText: 'Eliminar',
      destructiveButtonClicked: function () {
        console.log('ELIMINAR');
        return true; //Close the model?
      },
      buttons: [
        {text: '<i class="icon ion-share"></i> Share'}, //Index = 0
        {text: '<i class="icon ion-arrow-move"></i> Mover'}, //Index = 1
      ],
      buttonClicked: function (index) {
        switch (index) {
          case 0 :
            console.log('Share');
            return true;
          case 1 :
            console.log('Mover');
            return true;
        }
        
      }
    });
    /*$ionicActionSheet.show({
      titleText: 'ActionSheet Example',
      buttons: [
        {text: '<i class="icon ion-share"></i> Share'},
        {text: '<i class="icon ion-arrow-move"></i> Move'},
      ],
      destructiveText: 'Delete',
      cancelText: 'Cancel',
      cancel: function () {
        console.log('CANCELLED');
      },
      buttonClicked: function (index) {
        console.log('BUTTON CLICKED', index);
        return true;
      },
      destructiveButtonClicked: function () {
        console.log('DESTRUCT');
        return true;
      }
    });*/
  };
})

