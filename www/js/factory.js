var pago_movil = angular.module('pago_movil');
pago_movil.factory('usersFactory', ['$http', function ($http) {

  // var urlBase = 'http://www.criptozona.com/api';
  // var urlBase = 'http://josue-debian.ddns.net:8000/pago_movil/public/api';
  var urlBase = 'http://192.168.0.10:8000/api';
  var usersFactory = {};

  usersFactory.loginUser = function (logiData) {
    return $http.post(urlBase+'/user/login', logiData);
  };
  
  usersFactory.createUser = function (createData) {
    return $http.post(urlBase+'/user/create', createData);
  };
  
  usersFactory.loginHash = function (logiData) {
    return $http.post(urlBase+'/user/log_hash', logiData);
  };
  
  usersFactory.getCuentas = function (userId) {
    return $http.get(urlBase + '/cuentas', {params: {user_id: userId}});
  };

  usersFactory.transaccionPagar = function (text) {
    return $http.post(urlBase+'/transaccion/pagar', text);
  };

  usersFactory.asociarCuenta = function (cuenta) {
    return $http.post(urlBase+'/asociar', cuenta);
  };
  return usersFactory;
}]);

pago_movil.factory('Data', function () {
  var email = {};
  return {
    getEmail: function () {
      return email;
    },
    setEmail: function (emailparameter) {
      email = emailparameter;
    }
  };
})

