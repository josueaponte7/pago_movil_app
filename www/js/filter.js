pago_movil.filter('format', function() {
  return function(value,d,x,s,c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (d > 0 ? '\\D' : '$') + ')',
    number = parseInt(value).toFixed(Math.max(0, ~~d))
    var r = (c ? number.replace('.', c) : number).replace(new RegExp(re, 'g'), '$&' + (s || ','))
    return r ;
  }
});
