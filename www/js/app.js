var pago_movil = angular.module('pago_movil', ['ionic', 'pago_movil.controllers', 'ngCordova']);

pago_movil.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
      
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

pago_movil.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
    
    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'AppCtrl'
    })
  
    .state('create', {
      url: '/create',
      templateUrl: 'templates/create.html',
      controller: 'AppCtrl'
    })
    
    .state('inicio', {
      url: '/inicio',
      templateUrl: 'templates/inicio.html',
      controller: 'InicioCtrl'
    })
  
    .state('confirmation', {
      url: '/confirmation',
      templateUrl: 'templates/confirmation.html',
      controller: 'ConfirmarCtrl'
    })
  
    .state('sincronizar', {
      url: '/sincronizar',
      templateUrl: 'templates/sincronizar.html',
      controller: 'SincronizarCtrl'
    })
    
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })
  
  
    .state('app.ini', {
      cache: false,
      url: '/ini',
      views: {
        'menuContent': {
          templateUrl: 'templates/ini.html',
          controller: 'IniCtrl'
        }
      }
    })
    
    .state('app.pagar', {
      cache: false,
      url: '/pagar',
      views: {
        'menuContent': {
          templateUrl: 'templates/pagar.html',
          controller: 'PagarCtrl'
        }
      }
    })
    
    
    .state('app.transacciones', {
      cache: false,
      url: '/transacciones',
      views: {
        'menuContent': {
          templateUrl: 'templates/transacciones.html',
          controller: 'TransaccionesCtrl'
        }
      }
    })
    
    .state('app.cuentas', {
      cache: false,
      url: '/cuentas',
      views: {
        'menuContent': {
          templateUrl: 'templates/cuentas.html',
          controller: 'CuentasCtrl'
        }
      }
    })
    
    .state('app.asociar', {
      cache: false,
      url: '/asociar',
      views: {
        'menuContent': {
          templateUrl: 'templates/asociar.html',
          controller: 'AsociarCtrl'
        }
      }
    })
    
    .state('app.generar', {
      cache: false,
      url: '/generar',
      views: {
        'menuContent': {
          templateUrl: 'templates/generar.html',
          controller: 'AsociarCtrl'
        }
      }
    })
  ;
  //$urlRouterProvider.otherwise('/login');
  $urlRouterProvider.otherwise('/inicio');
});
