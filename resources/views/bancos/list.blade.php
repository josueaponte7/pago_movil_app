<a href="{{route('crear')}}">Crear nuevo</a>
<table border="1">
    <thead>
        <tr>
            <td>Codigo</td>
            <td>Nombre</td>
            <td>Siglas</td>
            <td>Editar</td>
            <td>Eliminar</td>
        </tr>
    </thead>
<tbody>
@foreach ($bancos as $banco)
    <tr>
        <td>{{ $banco->codigo }}</td>
        <td>{{ $banco->nombre }}</td>
        <td>{{ $banco->siglas }}</td>
        <td><a href="{{route('edit', ['id' => $banco->id])}}">Editar</a></td>
        <td><a href="{{route('bancos.delete', ['id' => $banco->id])}}">Eliminar</a></td>
    </tr>
@endforeach
</tbody>
</table>
