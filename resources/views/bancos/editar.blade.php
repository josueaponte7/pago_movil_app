
<form method="POST" action="{{route('bancos.update', ['id' => collect(request()->segments())->last()])}}">
     {{ csrf_field() }}
    <p>Codigo<input type="text" name="codigo" id="codigo" value="{{ isset($banco->codigo) ? $banco->codigo : '' }}"></p>
    <p>Nombre<input type="text" name="nombre" id="nombre" value="{{ isset($banco->nombre) ? $banco->nombre : '' }}"></p>
    <p>Siglas<input type="text" name="siglas" id="siglas" value="{{ isset($banco->siglas) ? $banco->siglas : '' }}"></p>
    <button type="submit">Guardar</button>
</form>
@if (session()->has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif