
<form method="POST" action="{{ route('bancos.save') }}">
     {{ csrf_field() }}
    <p>Codigo<input type="text" name="codigo" id="codigo"></p>
    <p>Nombre<input type="text" name="nombre" id="nombre"></p>
    <p>Siglas<input type="text" name="siglas" id="siglas"></p>
    <button type="submit">Guardar</button>
</form>
@if (session()->has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif